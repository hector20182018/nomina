-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-11-2018 a las 01:32:14
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nomina`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` varchar(11) NOT NULL,
  `nombres` varchar(60) NOT NULL,
  `apellidos` varchar(60) NOT NULL,
  `direccion` varchar(30) DEFAULT NULL,
  `telefono` varchar(11) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `estadocivil` varchar(20) NOT NULL,
  `hijos` int(2) DEFAULT NULL,
  `salud` varchar(30) NOT NULL,
  `pension` varchar(30) NOT NULL,
  `foncesantia` varchar(30) NOT NULL,
  `salant` decimal(8,0) DEFAULT NULL,
  `salactual` decimal(8,0) NOT NULL,
  `fecha_inicio` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cargo` varchar(60) DEFAULT NULL,
  `foto` varchar(255) NOT NULL,
  `fecha_retiro` timestamp NULL DEFAULT NULL,
  `tipo_contrato` varchar(30) DEFAULT NULL,
  `ccostos` varchar(60) DEFAULT NULL,
  `quincena1` double NOT NULL,
  `quincena2` double NOT NULL,
  `horas_extras1` double NOT NULL,
  `horas_extras2` double NOT NULL,
  `auxtte1` double NOT NULL,
  `auxtte2` double NOT NULL,
  `devengado1` double NOT NULL,
  `devengado2` double NOT NULL,
  `devengado_mes` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `nombres`, `apellidos`, `direccion`, `telefono`, `correo`, `estadocivil`, `hijos`, `salud`, `pension`, `foncesantia`, `salant`, `salactual`, `fecha_inicio`, `cargo`, `foto`, `fecha_retiro`, `tipo_contrato`, `ccostos`, `quincena1`, `quincena2`, `horas_extras1`, `horas_extras2`, `auxtte1`, `auxtte2`, `devengado1`, `devengado2`, `devengado_mes`) VALUES
('10', 'Natalia', 'tatata', 'tatata', '123', 'tatata@.com', 'tatata', 0, 'tatata', 'tatatat', 'tattata', '800000', '1200000', '2018-11-11 05:00:00', 'tata', 'caracol.gif', '0000-00-00 00:00:00', '', '', 186666.67, 400000, 25000.02, 45833.37, 19399.33, 41570, 231066.02, 487403.37, 718469.39),
('100', 'Último ensayo', 'y si funciona', 'ya dejo ahí por hoy', '5599', 'ya@termine.com', 'por hoy', 0, 'Estoy cansado', 'Pero creo que', 'merezco descansar ya', '0', '3000000', '2018-11-12 05:00:00', 'creo que', 'naturaleza-paisaje-13.jpg', '0000-00-00 00:00:00', 'ya es suficiente', 'por este fin de semana', 400000, 200000, 187500, 125000, 0, 0, 587500, 325000, 912500),
('11', 'ensayo con fecha', 'Calendario', 'hhj', '45', 'njnh@.com', 'zbb', 0, 'ghgh', 'hghgh', 'hghg', '0', '250000', '0000-00-00 00:00:00', 'gghj', '', '0000-00-00 00:00:00', 'vvnbvnv', 'vvbn', 0, 0, 0, 0, 0, 0, 0, 0, 0),
('1110467571', 'Valentina', 'Castrillon', 'Panamá', '3152458796', 'Lavalec@hotmail.com', 'soltera', 0, 'Salud panamá', 'Colpensiones', 'Protección s.a', '7800000', '9750000', '2015-07-20 05:00:00', 'Secretaria', 'Valentina_Castrillon.jpg', '0000-00-00 00:00:00', 'Temporal', 'Panamá', 1950000, 3575000, 355468.75, 203125, 0, 0, 2305468.75, 3778125, 6083593.75),
('11128025636', 'Juliana', 'Bustamante', NULL, '3206696969', 'valentina@gmail.com', 'Separada', 0, 'Cruz Blanca', 'Colfondos', 'Skandia', '2200000', '2420000', '2018-11-04 05:00:00', 'Ventas', 'fuente.gif', '0000-00-00 00:00:00', 'indefinido', 'ventas', 887333.33, 968000, 50416.68, 0, 0, 0, 937750.01, 968000, 2228000),
('11128025637', 'Kelly Milena', 'Úsuga', NULL, '3206676996', 'lamile@gmail.com', 'Separada', 2, 'Savia salud eps', 'Colfondos', 'Protección s.a', '594000', '712800', '2014-12-16 05:00:00', 'ventas', 'Milena_Usuga.jpg', NULL, 'obra o labor', 'ventas', 71280, 356400, 22275, 0, 8314, 0, 101869, 0, 722494.67),
('11128025699', 'Valentina', 'Úsuga', NULL, '6969696969', 'valen@gmail.com', 'Soltera', 0, 'Sura eps', 'Colfondos', 'Skandía', '600000', '660000', '2018-11-04 05:00:00', 'atención al público', 'Valentina_Usuga.jpg', NULL, 'fijo', 'ventas', 242000, 220000, 6875, 6875, 30484.67, 0, 279359.67, 226875, 606875),
('11206548932', 'Sara Melissa', 'Cano', 'Buenos aires', '3201456321', 'saritakano@gmail.com', 'Divorciada', 0, 'Sura', 'Colpensiones', 'Protección', '960000', '1536000', '2017-12-20 05:00:00', 'atención al cliente', 'Sarita_Cano.jpg', '0000-00-00 00:00:00', 'fijo', 'ventas', 768000, 768000, 0, 0, 41570, 0, 809570, 0, 1427713.33),
('1125469339', 'Stefanny', 'Villegas', 'Aranjuéz', '3202569415', 'steffy@gmail.com', 'Unión libre', 0, 'Savia salud eps', 'Protección s.a', 'Colfondos', '1800000', '1980000', '2012-12-02 05:00:00', 'Atención al público', 'Stefanny_Villegas.jpg', '0000-00-00 00:00:00', 'Indefinido', 'Ventas', 990000, 990000, 0, 0, 25000, 0, 0, 0, 2005000),
('11256321569', 'Melissa', 'Walteros Cruz', 'Buenos Aires', '3135134800', 'melissawalteros20002018@gmail.', 'Unión libre', 0, 'Universitaria', 'Porvenir s.a', 'Porvenir s.a', '800000', '840000', '2018-09-06 05:00:00', 'atención al cliente', 'Melissa_Walteros1.jpg', '0000-00-00 00:00:00', 'obra o labor', 'ventas', 84000, 308000, 21875, 17500, 0, 30484.67, 105875, 355984.67, 713484.67),
('1125632969', 'Laura', 'Harby', 'Venezuela', '3216549632', 'luvene@gmail.com', 'Separada', 3, 'Savia salud eps', 'Protección s.a', 'Skandía', '750000', '1125000', '2018-08-25 05:00:00', 'Ventas', 'Laura.jpg', '0000-00-00 00:00:00', 'Temporal', 'Atención al cliente', 262500, 562500, 0, 0, 14000, 0, 0, 0, 839000),
('12', 'nhfj', 'nnn', 'nnn', '12', 'lll@m.com', 'jj', 2, 'nn', 'nn', 'nn', '10', '950000', '2018-11-11 05:00:00', 'hh', 'fuente.gif', '0000-00-00 00:00:00', 'oo', 'oo', 475000, 1.33, 19791.68, 0, 41570, 0, 536361.68, 1.33, 694.43),
('1205145874', 'Marisol', 'Acuña', 'San Javier', '3157679384', 'mary@sol.com', 'Viuda', 1, 'Savia Salud eps', 'Porvenir s.a', 'Colfondos', '800000', '1600000', '2017-08-12 05:00:00', 'Atención al público', 'MARISOL.jpg', '0000-00-00 00:00:00', 'indefinido', 'ventas', 480000, 373333.33, 66666.64, 0, 0, 0, 546666.64, 373333.33, 760666.85),
('123456789', 'Ensayo', 'Ensayando', 'a ver si funciona', '12345678911', 'ensayar@hotmail.com', 'y funcionó', 0, 'Famisanar eps', 'Protección s.a', 'Porvrnir s.a', '800000', '950000', '2018-02-02 05:00:00', 'Sólo rnsayo', 'caracol.gif', '0000-00-00 00:00:00', 'Ensayado', 'Ya ensayado', 934166.67, 190000, 24739.6, 0, 81754.33, 0, 1040660.6, 190000, 1230660.6),
('1234567891', 'ghhgl', 'nbkn', 'kmm', '12', 'jkgjk@.com', 'tat', 0, 'jjd', 'hsdh', 'hh', '550', '550550', '2018-11-12 21:32:11', '', 'fondo.gif', '0000-00-00 00:00:00', '', '', 275275, 25, 25807.05, 0.26, 41570, 0, 342652.05, 25.26, 342677.31),
('12345678910', 'Ensayo', 'a ver si funciona', 'ensayando', '12345678910', 'ensayo@hotmail.com', 'ensayado', 5, 'sura', 'Protección', 'Colfondos', '100000', '200000', '2018-11-18 23:13:13', 'Ventas', 'fuente.gif', '0000-00-00 00:00:00', 'Indefinido', 'Ventas', 100000, 13333.33, 17708.39, 1041.67, 41570, 0, 159278.39, 14375, 173653.39),
('12345678911', 'Ensayo', 'Ensayando', 'a ver si funciona', '12345678936', 'ensayar@hotmail.com', 'y funcionó', 0, 'Famisanar eps', 'Protección s.a', 'Porvrnir s.a', '800000', '950000', '2018-11-18 23:13:24', 'Sólo rnsayo', 'caracol.gif', '0000-00-00 00:00:00', 'Ensayado', 'Ya ensayado', 475000, 348333.33, 19791.68, 9895.84, 41570, 0, 536361.68, 358229.17, 894590.85),
('1234567895', 'Ensayo', 'Ensayando', 'a ver si funciona', '1234567891', 'ensayar@hotmail.com', 'y funcionó', 0, 'Famisanar eps', 'Protección s.a', 'Porvrnir s.a', '800000', '950000', '2018-11-18 23:13:48', 'Sólo rnsayo', 'caracol.gif', '0000-00-00 00:00:00', 'Ensayado', 'Ya ensayado', 475000, 475000, 14843.76, 4947.92, 41570, 41570, 531413.76, 521517.92, 1052931.68),
('15', 'hola', 'estoy muy cansado', 'y necesito un descanso', '315264236', 'necesito descansar', 'por que', 0, 'yo creo que es suficiente', 'por este fin de semana', 'y necesito', '0', '3000000', '2018-11-18 23:15:20', 'dormir', '', '0000-00-00 00:00:00', 'para recuperarme', 'mmmm', 700000, 1500000, 390625, 31250, 0, 0, 1090625, 1531250, 2621875),
('20', 'hola', 'estoy muy cansado', 'y necesito un descanso', '315264236', 'necesito descansar', 'por que', 0, 'yo creo que es suficiente', 'por este fin de semana', 'y necesito', '0', '3000000', '2018-11-18 23:16:48', 'dormir un poco', '', '0000-00-00 00:00:00', 'para recuperarme', 'de tanto estudiar', 1100000, 1500000, 31250, 93750, 0, 0, 1131250, 1593750, 2725000),
('2020', 'Ana María', 'Valderrama ospina', 'sasa', '3254624158', 'sasa@.com', 'soltera', 0, 'Sura', 'Colpensiones', 'Protección s.a', '0', '1100000', '2018-11-11 05:00:00', 'sasa', 'naturaleza-paisaje-13.jpg', '0000-00-00 00:00:00', 'sasa', 'sasa', 0, 0, 0, 0, 0, 0, 0, 0, 0),
('234567891', 'Ensayo', 'Ensayando', 'a ver si funciona', '12345678936', 'ensayar@hotmail.com', 'y funcionó', 0, 'Famisanar eps', 'Protección s.a', 'Porvrnir s.a', '800000', '950000', '2018-02-02 05:00:00', 'Sólo rnsayo', 'caracol.gif', '0000-00-00 00:00:00', 'Ensayado', 'Ya ensayado', 0, 0, 0, 0, 0, 0, 0, 0, 0),
('2345678911', 'Ensayo', 'Ensayando', 'a ver si funciona', '12345678936', 'ensayar@hotmail.com', 'y funcionó', 0, 'Famisanar eps', 'Protección s.a', 'Porvrnir s.a', '800000', '950000', '2018-02-02 05:00:00', 'Sólo rnsayo', 'caracol.gif', '0000-00-00 00:00:00', 'Ensayado', 'Ya ensayado', 0, 0, 0, 0, 0, 0, 0, 0, 0),
('43875180', 'Alba Luz', 'Aristizabal Serna', 'Poblado', '3216549632', 'albita89@gmail.com', 'Casada', 2, 'Coomeva', 'Colpensiones', 'Protección s.a', '500000', '6000000', '1983-02-17 05:00:00', 'Ingeniera auxiliar', 'fondo.gif', '0000-00-00 00:00:00', 'Indefinido', 'Administración', 1400000, 3000000, 0, 0, 14000, 0, 0, 0, 0),
('70725451', 'Héctor de Jesús', 'Pavas', 'Bello (Antioquia)', '3206676999', 'hector@gmail.com', 'Soltero', 0, 'Coomeva eps', 'Protección s.a', 'Protección s.a', '2500000', '4200000', '2017-12-01 05:00:00', 'Auditor', 'Hector4.jpg', '0000-00-00 00:00:00', 'indefinido', 'Administración', 1540000, 2100000, 109375, 0, 0, 0, 1649375, 0, 0),
('98570202', 'Didier Arley', 'Marín Bedoya', 'Bello', '3106258888', 'didier@gmail.com', 'Casado', 1, 'Coomeva eps', 'Colpensiones', 'Porvenir s.a', '4500000', '6000000', '2001-02-17 05:00:00', 'Ingeniero residente jefe de laboratorio', 'burbuja.jpg', '0000-00-00 00:00:00', 'Indefinido', 'Laboratorio', 1400000, 3000000, 0, 0, 0, 0, 1400000, 0, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
