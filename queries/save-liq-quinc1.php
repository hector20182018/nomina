<?php

/* 
 Modelo para guardar inserciones y actualizaciones
 */

if(!empty($_POST['txtName']) && !empty($_POST['txtApellido'])){
    include '../config/configuration.php';
    $con->connect();
    
    $salarioactual = trim(utf8_decode($_POST['txtSalario']));    
    $horas = trim(utf8_decode($_POST['txtprimeraquincena']));
    $hed = trim(utf8_decode($_POST['txtextras1']));
    $salminimo = 781242;
    $auxttequin1 = 83140;
    
    $quincenauno = round($salarioactual/240 * $horas, 2);
    $vhed = round($salarioactual/240*1.25, 2)*$hed;
    if ($salarioactual < $salminimo*2){
        $vrauxtte1= round($auxttequin1 / 240 * $horas, 2);        
    } else {
        $vrauxtte1 = 0;       
    }
    
    $totaldevengado1 = round($quincenauno+$vrauxtte1+$vhed, 2);
        
    
    
    
    if ($_POST['txtId'] ==''){
        $query = "INSERT INTO empleados(nombres,apellidos,estadocivil,correo,telefono,foto,fecha_inicio,salud,pension,foncesantia,salactual)
                VALUES('')";//ECT.
        $operation = "insertado";
    }else{
        $id = $_POST['txtId'];
        $query = "
            UPDATE empleados
            SET quincena1 = '$quincenauno',
                salactual = '$salarioactual',
                horas_extras1 = '$vhed',    
                auxtte1 = '$vrauxtte1',
                devengado1 = '$totaldevengado1'    
            WHERE id = '$id'
                ";
        $operation = "actualizada";
                
    }
    $con->setQuery($query);
    if ($con->getQuery()){
        $message = "quincena $operation correctamente";
    }else{
        $message = "La quincena no pudo ser $operation";
    }
        
    
} else {
    $message = "Ingrese la información solicitada";
  }
  echo json_encode(['message' => $message]);

