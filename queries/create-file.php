<?php

/* 
 script PHP para crear un archivo txt con nombre indicado por el usuario
 */

if(!empty($_POST['txtName']) && !empty($_POST['txaText'])){
    $fileName = '../assets/files/' . $_POST['txtName'] . '.txt';
    $fileContent = $_POST['txaText'];
    $file = fopen($fileName, 'w+');
    fwrite($file, $fileContent);
    fclose($file);
    $message = "archivo creado correctamente.";
}else{
    $message ="No suministró datos suficientes para crear el archivo";
}
echo json_encode(['message' => $message]);