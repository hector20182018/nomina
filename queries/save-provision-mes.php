<?php

/* 
 Modelo para guardar inserciones y actualizaciones
 */

if(!empty($_POST['txtName']) && !empty($_POST['txtApellido'])){
    include '../config/configuration.php';
    $con->connect();
    
    $quince1 = trim(utf8_decode($_POST['txtPrimquincena']));
    $quince2 = trim(utf8_decode($_POST['txtSegquincena']));
    $extrass1 = trim(utf8_decode($_POST['txtExtr1']));
    $extrass2 = trim(utf8_decode($_POST['txtExtr2']));
    $auxttequince1 = trim(utf8_decode($_POST['txttte1']));
    $auxttequince2 = trim(utf8_decode($_POST['txttte2']));  
    
    
    $total_devengadomes = round($quince1 + $quince2 + $extrass1 + $extrass2 + $auxttequince1 + $auxttequince2, 2);
        
    
    
    
    if ($_POST['txtId'] ==''){
        $query = "INSERT INTO empleados(nombres,apellidos,estadocivil,correo,telefono,foto,fecha_inicio,salud,pension,foncesantia,salactual)
                VALUES('')";//ECT.
        $operation = "insertado";
    }else{
        $id = $_POST['txtId'];
        $query = "
            UPDATE empleados
            SET devengado_mes = '$total_devengadomes'    
            WHERE id = '$id'
                ";
        $operation = "cerrado";
                
    }
    $con->setQuery($query);
    if ($con->getQuery()){
        $message = "Mes $operation correctamente, el total devengado en el mes de esta persona "
                . "fué de $".number_format($total_devengadomes);
    }else{
        $message = "El mes no pudo ser $operation";
    }
        
    
} else {
    $message = "Ingrese la información solicitada";
  }
  echo json_encode(['message' => $message]);

