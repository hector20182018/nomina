<?php

/* 
 Modelo para guardar inserciones y actualizaciones
 */

if(!empty($_POST['txtName']) && !empty($_POST['txtApellido'])){
    include '../config/configuration.php';
    $con->connect();
    
    $salarioactual = trim(utf8_decode($_POST['txtSalario']));    
    $horas2 = trim(utf8_decode($_POST['txtSegundaquincena']));
    $hed2 = trim(utf8_decode($_POST['txtExtras2']));
    $salminimo = 781242;
    $auxttequin2 = 83140;
    
    $quincenados = round($salarioactual/240 * $horas2, 2);
    $vhed2 = round($salarioactual/240*1.25, 2)*$hed2;
    if ($salarioactual < $salminimo*2){
        $vrauxtte2= round($auxttequin2 / 240 * $horas2, 2);        
    } else {
        $vrauxtte2 = 0;       
    }
    
    $totaldevengado2 = round($quincenados+$vrauxtte2+$vhed2, 2);
        
    
    
    
    if ($_POST['txtId'] ==''){
        $query = "INSERT INTO empleados(nombres,apellidos,estadocivil,correo,telefono,foto,fecha_inicio,salud,pension,foncesantia,salactual)
                VALUES('')";//ECT.
        $operation = "insertado";
    }else{
        $id = $_POST['txtId'];
        $query = "
            UPDATE empleados
            SET quincena2 = '$quincenados',
                salactual = '$salarioactual',
                horas_extras2 = '$vhed2',    
                auxtte2 = '$vrauxtte2',
                devengado2 = '$totaldevengado2'    
            WHERE id = '$id'
                ";
        $operation = "actualizada";
                
    }
    $con->setQuery($query);
    if ($con->getQuery()){
        $message = "quincena $operation correctamente";
    }else{
        $message = "La quincena no pudo ser $operation";
    }
        
    
} else {
    $message = "Ingrese la información solicitada";
  }
  echo json_encode(['message' => $message]);

