<?php

/* 
 * Modelo para listar los empleados de la base de datos
 */

include '../config/configuration.php';
$entity = "empleados";
$con->connect();

$query = "SELECT * FROM $entity";

$con->setQuery($query);
$nreg = $con->totalRecords();

$table = "";
$table .= "<table border='1'>";
$table .= "<caption align='bottom'>";
$table .= "Total registros:";
$table .= "<span class=''>$nreg</span>";
$table .= "</caption>";
$table .= "<tr>
    		<th>Identificación</th>
    		<th>Nombres</th>
    		<th>Apellidos</th>
                <th>Cargo</th>
                <th>Segunda quincena</th>                
                <th>Valor extras</th>
                <th>Auxilio de transp.</th>
                <th>Total devengado</th>
                <th>Salario actual</th>                 
    		<th>Liquidar</th>
    		</tr>";

while($row = $con->getArrayRecord()){
	$id = $row['id'];
	$table .= "<tr>";
	$table .= "<td style='text-align:right'>" . number_format($row['id']) . "</td>";
	$table .= "<td>" . utf8_encode($row['nombres']) . "</td>";
        $table .= "<td>" . utf8_encode($row['apellidos']) . "</td>";
        $table .= "<td>" . utf8_encode($row['cargo']) . "</td>";	     
	$table .= "<td style='text-align:right'>" . number_format($row['quincena2'],2, ",", ".") . "</td>";	
        $table .= "<td style='text-align:right'>" . number_format($row['horas_extras2'],2, ",", ".") . "</td>";	
        $table .= "<td style='text-align:right'>" . number_format($row['auxtte2'],2, ",", ".") . "</td>";
        $table .= "<td style='text-align:right'>" . number_format($row['devengado2'],2, ",", ".") . "</td>";
	$table .= "<td style='text-align:right'>" . number_format($row['salactual'],2, ",", ".") . "</td>";
        
        
	$table .= "<td>";
	$table .= "<a href='#!' onclick='updateRecord(\"$entity\", \"$id\");'>Editar</a>";
	$table .= "</td>";

	$table .= "</tr>";
}

$table .= "</table>";

$con->freeQuery();
$con->closeConnection();

$arrayResult = ['table' => $table];
echo json_encode($arrayResult);