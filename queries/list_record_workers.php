<?php

/* 
 * Modelo para listar los empleados de la base de datos
 */

include '../config/configuration.php';
$entity = "empleados";
$con->connect();

$query = "SELECT * FROM $entity";

$con->setQuery($query);
$nreg = $con->totalRecords();

$table = "";
$table .= "<table border='1'>";
$table .= "<caption align='bottom'>";
$table .= "Total registros:";
$table .= "<span class=''>$nreg</span>";
$table .= "</caption>";
$table .= "<tr>
    		<th>Identificación</th>
    		<th>Nombres</th>
    		<th>Apellidos</th>
                <th>Cargo</th>                
    		</tr>";

while($row = $con->getArrayRecord()){
	$id = $row['id'];
	$table .= "<tr>";
	$table .= "<td style='text-align:right'>" . number_format($row['id']) . "</td>";
	$table .= "<td>" . utf8_encode($row['nombres']) . "</td>";
        $table .= "<td>" . utf8_encode($row['apellidos']) . "</td>";
        $table .= "<td>" . utf8_encode($row['cargo']) . "</td>";	
	$table .= "</tr>";
}

$table .= "</table>";

$con->freeQuery();
$con->closeConnection();

$arrayResult = ['table' => $table];
echo json_encode($arrayResult);