<?php

/* 
 * Modelo para listar los empleados de la base de datos
 */

include '../config/configuration.php';
$entity = "empleados";
$con->connect();

$query = "SELECT * FROM $entity";

$con->setQuery($query);
$nreg = $con->totalRecords();

$table = "";
$table .= "<table border='1'>";
$table .= "<caption align='bottom'>";
$table .= "Total registros:";
$table .= "<span class=''>$nreg</span>";
$table .= "</caption>";
$table .= "<tr>
    		<th>Identificación</th>
    		<th>Nombres</th>
    		<th>Apellidos</th>
                <th>Est. civil</th>
                <th>E-mail</th>
                <th>Teléfono</th> 
                <th>Foto</th> 
    		<th>Fecha ingreso</th>
                <th>Salud</th>
                <th>Pensión</th>
                <th>F. cesantías</th>
                <th>Salario</th>                 
    		<th>Eliminar</th>
    		<th>Editar</th>
    		</tr>";

while($row = $con->getArrayRecord()){
	$id = $row['id'];
	$table .= "<tr>";
	$table .= "<td style='text-align:right'>" . number_format($row['id']) . "</td>";
	$table .= "<td>" . utf8_encode($row['nombres']) . "</td>";
        $table .= "<td>" . utf8_encode($row['apellidos']) . "</td>";
        $table .= "<td>" . utf8_encode($row['estadocivil']) . "</td>";
	$table .= "<td>" . $row['correo'] . "</td>";
        $table .= "<td style='text-align:right'>" . $row['telefono'] . "</td>";        
	$table .= "<td>";
	$table .= "<img src='../assets/imagenes/" . $row['foto'] . "' width='50' height='50' />";
	$table .= "</td>";
	$table .= "<td>" . $row['fecha_inicio'] . "</td>";
        $table .= "<td>" . utf8_encode($row['salud']) . "</td>";
        $table .= "<td>" . utf8_encode($row['pension']) . "</td>";
        $table .= "<td>" . utf8_encode($row['foncesantia']) . "</td>";
	$table .= "<td style='text-align:right'>" . number_format($row['salactual'],2, ",", ".") . "</td>"; 
        
	$table .= "<td>";
	$table .= "<a href='#!' onclick='if(confirm(\"¿Está seguro?\")){deleteRecord(\"$entity\", \"$id\");}'>Eliminar</a>";
	$table .= "</td>";

	$table .= "<td>";
	$table .= "<a href='#!' onclick='updateRecord(\"$entity\", \"$id\");'>Editar</a>";
	$table .= "</td>";

	$table .= "</tr>";
}

$table .= "</table>";

$con->freeQuery();
$con->closeConnection();

$arrayResult = ['table' => $table];
echo json_encode($arrayResult);