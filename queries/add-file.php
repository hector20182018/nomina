<?php

/* 
 script PHP para adicionar contenido a un archivo txt con nombre indicado por el usuario y ya existente
 */

if(!empty($_POST['txtName']) && !empty($_POST['txaText'])){
    $fileName = '../assets/files/' . $_POST['txtName'] . '.txt';
    if (file_exists($fileName)){
        $fileContent = "\n".$_POST['txaText'];
    $file = fopen($fileName, 'a+');
    fwrite($file, $fileContent);
    fclose($file);
    $message = "archivo modificado correctamente.";
    }  else {
       $message = "El archivo especificado no existe";
    }
        
}else{
    $message ="No suministró datos suficientes para modificar el archivo";
}
echo json_encode(['message' => $message]);