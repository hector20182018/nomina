<!DOCTYPE html>
<!--
Gestión de usuarios con Ajax (AJaX)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Empleados BD</title>
        <link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <div align=center>
    <body style="background: #cfc">
        
        <h3 style="color:  #003eff">LISTADO DE EMPLEADOS</h3>

        <div id="form">
            <form name="frmData" id="frmData">
            
            </table>  
                
                
                <br><br>
                
                
                <a href="inicio.php"><b style= " color:  #0069d9; background: moccasin; border:6px outset blanchedalmond; font-size: 10pt"><i>Inicio</i></b></a>
                   
                </form>
            
            
            
        </div>
        <br>
        <div id="resultRecords"></div>

        <div id="dialog" title="Mensaje del sistema"></div>
        
        <script src="../assets/js/jquery-3.3.1.js"></script>
        <script src="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

        <script type="text/javascript">
        	$(function(){
                list_records_employees();
                
                
            });

        	function list_records_employees()
        	{
                $.ajax({
                    'url': '../queries/list_record_workers.php',
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#resultRecords').html(data.table);
                    }
                });
        	}
            
            
            function getRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/seek_employees.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        if(data.message === ''){
                            
                            $('#txtId').val(data.id);
                            $('#txtName').val(data.name);
                            $('#txtApellido').val(data.apellidoss);                                                   
                            $('#txtFotos').val(data.fotos);
                            $('#txtSalanterior').val(data.salanterior); 
                            $('#txtSalario').val(data.salarioactual);                            
                        }else{
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        
                    }
                });
            }
            
            function saveRecord()
            {                          
                 $.ajax({
                    'url': '../queries/save-salary.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                        list_records();
                    }
                }); 
                
                
                
        }
        function insertRecord()
            {
                 $.ajax({
                    'url': '../queries/insert-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                        list_records();
                    }
                });    
            }
          
        </script>
    </body>
    </div>
</html>
