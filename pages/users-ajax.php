<!DOCTYPE html>
<!--
Gestión de usuarios con Ajax (AJaX)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Nómina BD</title>
        <link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <div align=center>
    <body style="background: #cfc">
        
        <h3 style="color:  #003eff">Estoy ensayando una base de datos llamada nomina</h3>

        <div id="form">
            <form name="frmData" id="frmData">
                <label for="txtId">Identificación</label>
                <input type="number" name="txtId" id="txtId" style= "background: paleturquoise">
                <label for="txtName">Nombres</label>
                <input type="text" name="txtName" id="txtName" maxlength="100" style= "background: paleturquoise">
                <label for="txtApellido">Apellidos</label>
                <input type="text" name="txtApellido" id="txtApellido" maxlength="100" style= "background: paleturquoise">
                <label for="txtEstcivil">Est. civil</label>
                <input type="text" name="txtEstcivil" id="txtEstcivil" maxlength="100" style= "background: paleturquoise">               
                <label for="txtEmail">E-mail</label>
                <input type="text" name="txtEmail" id="txtEmail" maxlength="100" style= "background: paleturquoise">                              
                <label for="txtTelefono">Teléfono</label>              
                <input type="text" name="txtTelefono" id="txtTelefono" maxlength="12" style= "background: paleturquoise">
                <br><br>
                <label for="txtFoto">Foto</label>
                <input type="text" name="txtFotos" id="txtFotos" maxlength="100" style= "background: paleturquoise">
                <label for="txtFecha">Fecha de Ingreso</label>
                <input type="datetime" name="txtFecha" id="txtFecha" maxlength="100" style= "background: paleturquoise">
                <label for="txtSalud">Salud</label>              
                <input type="text" name="txtSalud" id="txtSalud" maxlength="100" style= "background: paleturquoise">
                <label for="txtPension">Pensión</label>              
                <input type="text" name="txtPension" id="txtPension" maxlength="100" style= "background: paleturquoise">
                <label for="txtCesantias">F. Cesantías</label>              
                <input type="text" name="txtCesantias" id="txtCesantias" maxlength="100" style= "background: paleturquoise">
                <label for="txtSalario">Salario</label>              
                <input type="number" name="txtSalario" id="txtSalario" style= "background: paleturquoise">
                
                
                
                <br><br>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" name="btnSave" id="btnSave" value="Guardar" style= "background: skyblue">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          
                <input type="reset" value="Restablecer" style="background: whitesmoke">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <!--<input type="button" name="btnInsert" id="btnInsert" value="insertar" style= "background: moccasin">-->
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <a href="ingresar_registros_ajax.php"><b style="color: green"><i>Nuevo Registro</i></b></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <a href="incremento_salario.php"><b style="color: #2E64FE"><i>Incrementear salario</i></b></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <a href="inicio.php"><b style= " color:  #0069d9; background: moccasin; border:6px outset blanchedalmond; font-size: 10pt"><i>Inicio</i></b></a>
            </form>
            
            
            
        </div>
        <br>
        <div id="resultRecords"></div>

        <div id="dialog" title="Mensaje del sistema"></div>
        
        <script src="../assets/js/jquery-3.3.1.js"></script>
        <script src="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

        <script type="text/javascript">
        	$(function(){
                listRecords();
                $('#btnSave').click(function(){
                    saveRecord();
                });
                
                $('#btnInsert').click(function(){
                    insertRecord();
                });
                
            });

        	function listRecords()
        	{
                $.ajax({
                    'url': '../queries/userList.php',
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#resultRecords').html(data.table);
                    }
                });
        	}
            
            
            function deleteRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/deleteRecordAjax.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog();
                        listRecords();
                    }
                });
            }


            function updateRecord(entity, id)
            {
                $('#dialog').html('¿Está seguro?');
                $('#dialog').dialog({
                    autoOpen: true,
                    modal: true,
                    buttons: {
                        'Aceptar': function(){
                            getRecord(entity, id);
                            $(this).dialog('close');
                        },
                        'Cerrar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
            }


            function getRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/findRecord.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        if(data.message === ''){
                            $('#txtId').val(data.id);
                            $('#txtName').val(data.name);
                            $('#txtApellido').val(data.apellidoss);
                            $('#txtEstcivil').val(data.estcivil);
                            $('#txtEmail').val(data.correos);
                            $('#txtTelefono').val(data.telefonos);                            
                            $('#txtFotos').val(data.fotos);
                            $('#txtFecha').val(data.fecha);
                            $('#txtSalud').val(data.eps);
                            $('#txtPension').val(data.pensiones);
                            $('#txtCesantias').val(data.cesantias);
                            $('#txtSalario').val(data.salarioactual);                            
                        }else{
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        
                    }
                });
            }
            
            function saveRecord()
            {
                 $.ajax({
                    'url': '../queries/save-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                        listRecords();
                    }
                }); 
                
                
                
        }
        function insertRecord()
            {
                 $.ajax({
                    'url': '../queries/insert-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                        listRecords();
                    }
                });    
            }
          
        </script>
    </body>
    </div>
</html>
