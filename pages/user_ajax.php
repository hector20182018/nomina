<!DOCTYPE html>
<!--
Gestion de usuarios con Ajax (AJaX)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Nómina</title>
    </head>
    <body>
        <h1>Empleados</h1>

        <div id="resultRecords"></div>
        
        <script src="../assets/js/jquery-3.3.1.js"></script>
        
        <script type="text/javascript">
            
        $(function()
        {
            listRecords();
        });
        
        function listRecords()
        {
            $.ajax({
                'url': '../queries/userList.php',
                'type': 'POST',
                'dataType': 'json',
                'success': function(data)
                {
                    $('#resultRecords').html(data.table);
                }
            });
        }
        
        </script>
    </body>
</html>
