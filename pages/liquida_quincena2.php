<!DOCTYPE html>
<!--
Gestión de usuarios con Ajax (AJaX)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Liquida nómina BD</title>
        <link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <div align=center>
    <body style="background: #cfc">
        
        <br>
        <a style= " color:   #088A68; background: moccasin; border:15px inset blanchedalmond; font-size: 18pt"><b><i>SEGUNDA QUINCENA</i></b></a>
        <br><br><br>
        <div id="form">
            <form name="frmData" id="frmData">
            <table border="" style="border-color: gold;">
            <tr>
            <th colspan="1">
                <label for="txtId">Identificación</label>
                <input type="number" name="txtId" id="txtId" style= "background: paleturquoise" readonly="">
                <label for="txtName">Nombres</label>
                <input type="text" name="txtName" id="txtName" maxlength="100" style= "background: paleturquoise" readonly="">
                <label for="txtApellido">Apellidos</label>
                <input type="text" name="txtApellido" id="txtApellido" maxlength="100" style= "background: paleturquoise" readonly="">
                <label for="txtSegundaquincena">Nro. de horas laboradas</label>
                <input type="text" name="txtSegundaquincena" id="txtSegundaquincena" maxlength="100" style= "background: paleturquoise">                                                                    
                <label for="txtSalario">Salario actual</label>              
                <input type="number" name="txtSalario" id="txtSalario" style= "background: paleturquoise" readonly="">
                <br>
                <label for="txtSalmin">Salario mínimo</label>              
                <input type="number" name="txtSalmin" id="txtSalmin" style= "background: paleturquoise" placeholder="781.242 mensual"readonly="">
                <label for="txtExtras2">H. extras diurnas</label>              
                <input type="number" name="txtExtras2" id="txtExtras2" style= "background: paleturquoise">
                
                <label for="txtAuxtrasnp2">Auxilio de transporte</label>              
                <input type="number" name="txtAuxtrasnp2" id="txtAuxtrasnp2" style= "background: paleturquoise"placeholder="83.140 mensual" readonly="">
                
                
            </th>   
            </tr>
            </table>
            
                
                
                <br><br>
                <a href="liquida_quincena1.php"><b style= " color:  #0069d9; background: #cfc; border:6px outset whitesmoke; font-size: 12pt"><i><<== Ir a la primera quincena</i></b></a>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" name="btnSave2" id="btnSave2" value="Guardar" style= "background: skyblue">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          
                <input type="reset" value="Restablecer" style="background: whitesmoke">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <!--<input type="button" name="btnInsert" id="btnInsert" value="insertar" style= "background: moccasin">-->
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                               
                <a href="inicio.php"><b style= " color:  #0069d9; background: moccasin; border:6px outset blanchedalmond; font-size: 12pt"><i>Inicio</i></b></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                <a href="prestaciones_sociales.php"><b style= " color:  green; background: #cfc; border:6px outset greenyellow; font-size: 12pt"><i>Ir a las prestaciones sociales</i></b></a>
                   
                </form>
            
            
            
        </div>
        <br>
        <div id="resultRecords"></div>

        <div id="dialog" title="Mensaje del sistema"></div>
        
        <script src="../assets/js/jquery-3.3.1.js"></script>
        <script src="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

        <script type="text/javascript">
        	$(function(){
                list_liquid_quinc2();
                $('#btnSave2').click(function(){
                    saveRecord2();
                });
                
                               
            });

        	function list_liquid_quinc2()
        	{
                $.ajax({
                    'url': '../queries/list_liquide_quinc2.php',
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#resultRecords').html(data.table);
                    }
                });
        	}
            
            
            function updateRecord(entity, id)
            {
                $('#dialog').html('¿Está seguro?');
                $('#dialog').dialog({
                    autoOpen: true,
                    modal: true,
                    buttons: {
                        'Aceptar': function(){
                            getRecord(entity, id);
                            $(this).dialog('close');
                        },
                        'Cerrar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
            }


            function getRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/seek_Record_liquid_quinc2.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        if(data.message === ''){
                            
                            $('#txtId').val(data.id);
                            $('#txtName').val(data.name);
                            $('#txtApellido').val(data.apellidoss);                                                   
                            
                            $('#txtSegundaquincena').val(data.quincenados); 
                            $('#txtSalario').val(data.salarioactual);
                            $('#txtSalmin').val(data.salminimo);
                            $('#txtExtras2').val(data.extras2);
                            $('#txtAuxtrasnp2').val(data.auxttequin2);
                        }else{
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        
                    }
                });
            }
            
            function saveRecord2()
            {                          
                 $.ajax({
                    'url': '../queries/save-liq-quinc2.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                      list_liquid_quinc2();   
                    }
                });               
               
        }
                  
        </script>
    </body>
    </div>
</html>
