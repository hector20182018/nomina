<!DOCTYPE html>
<!--
Gestión de usuarios con Ajax (AJaX)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Liquida nómina BD</title>
        <link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <div align=center>
    <body style="background: #cfc">
        
        <br>
        <a style= " color:   #088A68; background: moccasin; border:15px inset blanchedalmond; font-size: 18pt"><b><i>PROVISIÓN MENSUAL DE PRESTACIONES SOCIALES</i></b></a>
        <br><br><br>
        <div id="form">
            <form name="frmData" id="frmData">
            <table border="" style="border-color: gold;">
            <tr>
            <th colspan="0">            
                <label for="txtId">Identificación</label>
            <td>
                <input type="number" name="txtId" id="txtId" style= "background: paleturquoise" readonly="">
            </td>        
            <td>                
                <label for="txtName">Nombres</label>
            </td>
            <td>
                <input type="text" name="txtName" id="txtName" maxlength="100" style= "background: paleturquoise" readonly="">
            </td>
            <td>
                <label for="txtApellido">Apellidos</label>
            </td>
            <td>
                <input type="text" name="txtApellido" id="txtApellido" maxlength="100" style= "background: paleturquoise" readonly="">
            </td>
            <td>                    
                <label for="txtPrimquincena">Primera quincena</label>
            </td>
            <td>
                <input type="text" name="txtPrimquincena" id="txtPrimquincena" maxlength="100" style= "background: paleturquoise" readonly="">
            </td>
           
            <td>
                <label for="txtSegquincena">Segunda quincena</label>
            </td>
            <td>
                <input type="text" name="txtSegquincena" id="txtSegquincena" maxlength="100" style= "background: paleturquoise" readonly="">
            </td>
             <tr>
            <td>
                <label for="txtExtr1">extras quincena 1</label>
            </td>
            <td>
                <input type="number" name="txtExtr1" id="ttxtExtr1" style= "background: paleturquoise" readonly="">
            </td>
            <td>                
                <label for="txtExtr2">extras quincena 2</label>
            </td>
            <td>
                <input type="number" name="txtExtr2" id="ttxtExtr2" style= "background: paleturquoise" readonly="">
            </td>
            <td>
                <label for="txttte1">Transp. quincena 1</label>
            </td>
            <td>
                <input type="number" name="txttte1" id="txttte1" style= "background: paleturquoise" readonly="">
            </td>
            <td>
                <label for="txttte2">Transp. quincena 2</label>
            </td>
            <td>
                <input type="number" name="txttte2" id="txttte2" style= "background: paleturquoise" readonly="">
            </td> 
            <td>
                <label for="txtSalario">Salario actual</label>               
            </td>
            <td>
                <input type="number" name="txtSalario" id="txtSalario" style= "background: paleturquoise" readonly="">
             </td>      
            </th>   
            </tr>
            </table>
            
                
                
                <br><br>
                
        <br><br><br>
        <div id="form">
            <form name="frmData" id="frmData">
            
            
                
                
                <br><br>
                
                <a href="liquida_quincena1.php"><b style= " color:  #0069d9; background: #cfc; border:6px outset whitesmoke; font-size: 12pt"><i>Mirar la primera quincena</i></b></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <input type="reset" value="Restablecer" style="background: whitesmoke">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <input type="button" name="btnSave" id="btnSavec" value="Cerrar mes" style= "background: skyblue">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                <!--<input type="button" name="btnInsert" id="btnInsert" value="insertar" style= "background: moccasin">-->
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                               
                <a href="inicio.php"><b style= " color:  #0069d9; background: moccasin; border:6px outset blanchedalmond; font-size: 12pt"><i>Inicio</i></b></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <a href="liquida_quincena2.php"><b style= " color:  #0069d9; background: #cfc; border:6px outset palegoldenrod; font-size: 12pt"><i>Mirar la segunda quincena</i></b></a>
                   
            </form>
            
            
            
        </div>
        <br>
        <div id="resultRecords"></div>

        <div id="dialog" title="Mensaje del sistema"></div>
        
        <script src="../assets/js/jquery-3.3.1.js"></script>
        <script src="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

        <script type="text/javascript">
        	$(function(){
                list_proviciones();
                $('#btnSavec').click(function(){
                    saveRecordc();
                });
                
                               
            });

        	function list_proviciones()
        	{
                $.ajax({
                    'url': '../queries/list_provisones_mes.php',
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#resultRecords').html(data.table);
                    }
                });
        	}
            
            
            function updateRecord(entity, id)
            {
                $('#dialog').html('¿Está seguro?');
                $('#dialog').dialog({
                    autoOpen: true,
                    modal: true,
                    buttons: {
                        'Aceptar': function(){
                            getRecord(entity, id);
                            $(this).dialog('close');
                        },
                        'Cerrar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
            }


            function getRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/seek_Record_provisiones.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        if(data.message === ''){
                            
                            $('#txtId').val(data.id);
                            $('#txtName').val(data.name);
                            $('#txtApellido').val(data.apellidoss);
                            $('#txtPrimquincena').val(data.quince1);                            
                            $('#txtSegquincena').val(data.quince2);
                            $('#ttxtExtr1').val(data.extrass1);
                            $('#ttxtExtr2').val(data.extrass2);
                            $('#txttte1').val(data.auxttequince1);
                            $('#txttte2').val(data.auxttequince2);
                            $('#txtSalario').val(data.salarioactual);                                                    
                            
                        }else{
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        
                    }
                });
            }
            
            function saveRecordc()
            {                          
                 $.ajax({
                    'url': '../queries/save-provision-mes.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                      list_proviciones();   
                    }
                });               
               
        }
                  
        </script>
    </body>
    </div>
</html>
