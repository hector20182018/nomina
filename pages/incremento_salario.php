<!DOCTYPE html>
<!--
Gestión de usuarios con Ajax (AJaX)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Nómina BD</title>
        <link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <div align=center>
    <body style="background: #cfc">
        
        <h3 style="color:  #003eff">ACTUALIZACIÓN DE SALARIOS</h3>

        <div id="form">
            <form name="frmData" id="frmData">
            <table border="" style="border-color: gold;">
            <tr>
            <th colspan="1">
                <label for="txtId">Identificación</label>
                <input type="number" name="txtId" id="txtId" style= "background: paleturquoise" readonly="">
                <label for="txtName">Nombres</label>
                <input type="text" name="txtName" id="txtName" maxlength="100" style= "background: paleturquoise" readonly="">
                <label for="txtApellido">Apellidos</label>
                <input type="text" name="txtApellido" id="txtApellido" maxlength="100" style= "background: paleturquoise" readonly="">
                
                                                      
                <label for="txtSalanterior">Salario anterior</label>              
                <input type="number" name="txtSalanterior" id="txtSalanterior" style= "background: paleturquoise" readonly="">
                <label for="txtSalario">Salario actual</label>              
                <input type="number" name="txtSalario" id="txtSalario" style= "background: paleturquoise">
            </th>   
            </tr>
            </table>
            <table border="" style="border-color: gold;">
            <tr>
            <th colspan="1">                    
                <label for="txtIncremento">Incrementar el salario en</label>              
                <input type="number" name="txtIncremento" id="txtIncremento" style= "background: paleturquoise" maxlength="3" size="3">%
            </th>   
            </tr>
            </table>  
                
                
                <br><br>
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" name="btnSave" id="btnSave" value="Guardar" style= "background: skyblue">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          
                <input type="reset" value="Restablecer" style="background: whitesmoke">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <!--<input type="button" name="btnInsert" id="btnInsert" value="insertar" style= "background: moccasin">-->
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <a href="users-ajax.php"><b style="color: green"><i>Ver nuevo salario</i></b></a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                &nbsp;&nbsp;&nbsp;
                <a href="inicio.php"><b style= " color:  #0069d9; background: moccasin; border:6px outset blanchedalmond; font-size: 10pt"><i>Inicio</i></b></a>
                   
                </form>
            
            
            
        </div>
        <br>
        <div id="resultRecords"></div>

        <div id="dialog" title="Mensaje del sistema"></div>
        
        <script src="../assets/js/jquery-3.3.1.js"></script>
        <script src="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

        <script type="text/javascript">
        	$(function(){
                list_records();
                $('#btnSave').click(function(){
                    saveRecord();
                });
                
                $('#btnInsert').click(function(){
                    insertRecord();
                });
                
            });

        	function list_records()
        	{
                $.ajax({
                    'url': '../queries/list_record_changeSalary.php',
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#resultRecords').html(data.table);
                    }
                });
        	}
            
            
            function updateRecord(entity, id)
            {
                $('#dialog').html('¿Está seguro?');
                $('#dialog').dialog({
                    autoOpen: true,
                    modal: true,
                    buttons: {
                        'Aceptar': function(){
                            getRecord(entity, id);
                            $(this).dialog('close');
                        },
                        'Cerrar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
            }


            function getRecord(entity, id)
            {
                $.ajax({
                    'url': '../queries/seek_Record.php',
                    'data': {'entity': entity, 'id': id},
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        if(data.message === ''){
                            
                            $('#txtId').val(data.id);
                            $('#txtName').val(data.name);
                            $('#txtApellido').val(data.apellidoss);                                                   
                            $('#txtFotos').val(data.fotos);
                            $('#txtSalanterior').val(data.salanterior); 
                            $('#txtSalario').val(data.salarioactual);                            
                        }else{
                            $('#dialog').html(data.message);
                            $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                        }
                        
                    }
                });
            }
            
            function saveRecord()
            {                          
                 $.ajax({
                    'url': '../queries/save-salary.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                        list_records();
                    }
                }); 
                
                
                
        }
        function insertRecord()
            {
                 $.ajax({
                    'url': '../queries/insert-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                        list_records();
                    }
                });    
            }
          
        </script>
    </body>
    </div>
</html>
