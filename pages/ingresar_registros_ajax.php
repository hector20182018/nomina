<!DOCTYPE html>
<!--
Gestión de usuarios con Ajax (AJaX)
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Nuevos registros</title>
        <link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <div align=center>
    <body style="background: powderblue">
        
        <h3 style="color:  #003eff">Ingresar nuevos registros a la base de datos nomina</h3>

        <div id="form">
            <table border="" style="border-color: paleturquoise;">
            <tr>
            <th colspan="1">
            <form name="frmData" id="frmData">
                
            <table style="text-align: left">			                
            <tr>
                <th>
                    <label for="txtId">Identificación</label>
                </th>
                 <td>
                    <input type="number" name="txtId" id="txtId" style= "background: paleturquoise">
                </td>
                </tr>   
                <tr>
                    <th>
                    <label for="txtName">Nombres</label>
                    </th>
                    <td>
                    <input type="text" name="txtName" id="txtName" maxlength="100" style= "background: paleturquoise">
                    </td>                    
               
                    <th>                
                    <label for="txtApellido">Apellidos</label>
                    </th>
                    <td>
                        <input type="text" name="txtApellido" id="txtApellido" maxlength="100" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th>               
                    <label for="txtDirecc">Dirección</label>
                    </th>
                    <td>
                    <input type="text" name="txtDirecc" id="txtDirecc" maxlength="100" style= "background: paleturquoise"> 
                    </td>
                
                    <th> 
                    <label for="txtTelefono">Teléfono</label>
                    </th>
                    <td>
                    <input type="text" name="txtTelefono" id="txtTelefono" maxlength="12" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th> 
                    <label for="txtEmail">E-mail</label>
                    </th>
                    <td>
                    <input type="text" name="txtEmail" id="txtEmail" maxlength="100" style= "background: paleturquoise"> 
                    </td>
               
                    <th>               
                    <label for="txtEstcivil">Estado civil</label>
                    </th>
                    <td>
                    <input type="text" name="txtEstcivil" id="txtEstcivil" maxlength="100" style= "background: paleturquoise"> 
                    </td>
                </tr>
                <tr>
                    <th>               
                    <label for="txtHijos">Número de hijos</label>
                    </th>
                    <td>
                    <input type="text" name="txtHijos" id="txtHijos" maxlength="2" size="2" style= "background: paleturquoise"> 
                    </td>
                
                    <th> 
                    <label for="txtSalud">Eps</label>
                    </th>
                    <td>
                    <input type="text" name="txtSalud" id="txtSalud" maxlength="100" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th> 
                    <label for="txtPension">Pensión</label>
                    </th>
                    <td>
                    <input type="text" name="txtPension" id="txtPension" maxlength="100" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                    <label for="txtCesantias">Fondo Cesantías</label>
                    </th>
                    <td>
                    <input type="text" name="txtCesantias" id="txtCesantias" maxlength="100" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th> 
                        <label for="txtSalanterior" hidden="">Salario anterior</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtSalanterior" id="txtSalanterior" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                    <label for="txtSalario">Salario Actual</label>
                    </th>
                    <td>
                        <input type="number" name="txtSalario" id="txtSalario" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th> 
                    <label for="txtFecha">Fecha de Ingreso</label>
                    </th>
                    <td>
                    <input type="date" name="txtFecha" id="txtFecha" maxlength="100" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                    <label for="txtCargo">Cargo</label>
                    </th>
                    <td>
                        <input type="text" name="txtCargo" id="txtCargo" maxlength="100" style= "background: paleturquoise">
                    </td>
                </tr>
                
                <tr>
                    <th> 
                        <label for="txtFotos" hidden="">Foto</label>
                    </th>
                    <td>
                        <input hidden="" type="text" name="txtFotos" id="txtFotos" maxlength="100" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                        <label for="txtFechare" hidden="">Fecha de Retiro</label>
                    </th>
                    <td>
                        <input hidden="" type="datetime" name="txtFecharet" id="txtFecharet" maxlength="100" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th>               
                    <label for="txtContrato">Tipo de Contrato</label>
                    </th>
                    <td>
                    <input type="text" name="txtContrato" id="txtContrato" maxlength="100" style= "background: paleturquoise"> 
                    </td>
                
                    <th>               
                    <label for="txtCcostos">Centro de Costos</label>
                    </th>
                    <td>
                    <input type="text" name="txtCcostos" id="txtCcostos" maxlength="100" style= "background: paleturquoise"> 
                    </td>
                </tr>
                <tr>
                    <th> 
                        <label for="txtQuincena1" hidden="">Primera quincena</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtQuincena1" id="txtQuincena1" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                        <label for="txtQuincena2" hidden="">Segunda quincena</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtQuincena2" id="txtQuincena2" style= "background: paleturquoise">
                    </td>
                </tr>
                 <tr>
                    <th> 
                        <label for="txtExtras1" hidden="">Extras quincena 1</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtExtras1" id="txtExtras1" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                        <label for="txtExtras2" hidden="">Extras quincena 2</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtExtras2" id="txtExtras2" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th> 
                        <label for="txtTransporte1" hidden="">Auxilio transp.1</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtTransporte1" id="txtTransporte1" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                        <label for="txtTransporte2" hidden="">Auxilio transp.2</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtTransporte2" id="txtTransporte2" style= "background: paleturquoise">
                    </td>
                </tr>
                <tr>
                    <th> 
                        <label for="txtDevengo1" hidden="">Devengado Quin.2</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtDevengo1" id="txtDevengo1" style= "background: paleturquoise">
                    </td>
                
                    <th> 
                        <label for="txtDevengo2" hidden="">Devengado Quin.2</label>
                    </th>
                    <td>
                        <input hidden="" type="number" name="txtDevengo2" id="txtDevengo2" style= "background: paleturquoise">
                    </td>
                </tr>
                
                
                
                 </table>
            
            </th>
            </tr>
            </table>
                
            <br>          
                           
                 <input type="reset" value="Restablecer" style="background: whitesmoke">                                      
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="button" name="btnSave" id="btnSave" value="Guardar" style= "background: skyblue">
               
                &nbsp;&nbsp;&nbsp;&nbsp;
                
                <a href="users-ajax.php"><b style= " color:  #0069d9; background:  #bee5eb; border:6px outset blanchedalmond; font-size: 10pt"><i>Ver nuevos registros</i></b></a>
                <br><br>
                
                <a href="inicio.php"><b style= " color:  #0069d9; background: moccasin; border:6px outset blanchedalmond; font-size: 10pt"><i>Inicio</i></b></a>
            </form>      
            
            
        </div>
        
        <br>
        <div id="resultRecords"></div>

        <div id="dialog" title="Mensaje del sistema"></div>
        
        <script src="../assets/js/jquery-3.3.1.js"></script>
        <script src="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

        <script type="text/javascript">
        	$(function(){
                
                $('#btnSave').click(function(){
                    insertRecord();
                });
                
                               
            });

        	
            function updateRecord(entity, id)
            {
                $('#dialog').html('¿Está seguro?');
                $('#dialog').dialog({
                    autoOpen: true,
                    modal: true,
                    buttons: {
                        'Aceptar': function(){
                            getRecord(entity, id);
                            $(this).dialog('close');
                        },
                        'Cerrar': function(){
                            $(this).dialog('close');
                        }
                    }
                });
            }


            
            function saveRecord()
            {
                 $.ajax({
                    'url': '../queries/save-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                        
                    }
                }); 
                
                
                
        }
        function insertRecord()
            {
                 $.ajax({
                    'url': '../queries/insert-record.php',
                    'data': $('#frmData').serialize(),
                    'type': 'POST',
                    'dataType': 'json',
                    'success': function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    
                                }
                            }
                        });
                          
                       
                    }
                });    
            }
          
        </script>
    </body>
    </div>
</html>
