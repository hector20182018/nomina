<!DOCTYPE html>
<!--
Manejo de archivos con PHP
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Archivos en PHP</title>
        <link rel="stylesheet" type="text/css" href="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="../assets/css/estilos.css">
    </head>
    <body>
        
        <h1 class="text-center">Archivos en PHP</h1>
        <div class="container">
        <div class="cuadro_1">
            <div class="radio_del_borde">  
        
        <form name="frmFiles" id="frmFiles" class="text-center">
            <label for="txtName">Nombre archivo</label>
            <input type="text" name="txtName" id="txtName">.txt
            <br>
            <label for="txaText">Texto para el archivo</label>
            <textarea name="txaText" id="txaText"></textarea>
            
            <br>
            <label for="cboFiles">Archivos disponibles</label>
            <select name="cboFiles" id="cboFiles"></select>
            
            <label for="cboFiles">Total archivos disponibles</label>
            <input type="text" name="txtCountFiles" id="txtCountFiles" readonly="">
            
            
            <input type="button" name="btnDeleteFile" id="btnDeleteFile" value="Eliminar archivo" class="boton_2">
            
            
            
            
            <br>
            <input class="boton_1" type="button" name="btnCreateFile" id="btnCreateFile" value="Crear archivo">
            <input type="button" name="btnAddFile" id="btnAddFile" value="Adicionar texto al archivo" class="boton_1">
        </form>
        </div>        
        </div>
            </div>

        <div id="dialog" title="Mensaje del sistema"></div>

        <script src="../assets/js/jquery-3.3.1.js"></script>
         <script src="../assets/js/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
        
        <script type="text/javascript">
            $(function(){
                listFiles();
                $('#btnCreateFile').click(function(){
                    $.ajax({
                        url: '../queries/create-file.php',//url sin comillas-->
                        type: 'POST',
                        dataType: 'json',
                        data: $('#frmFiles').serialize(),
                        success: function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                           listFiles();
                        }
                    });
                });
            });
            
            $('#btnAddFile').click(function(){
                    $.ajax({
                        url: '../queries/add-file.php',//url sin comillas-->
                        type: 'POST',
                        dataType: 'json',
                        data: $('#frmFiles').serialize(),
                        success: function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                             listFiles();
                        }
                    });
                });
                
                function  listFiles()
                
                {
                    $.ajax({
                        url: '../queries/list-files.php',//url sin comillas-->
                        type: 'POST',
                        dataType: 'json',
                        data: $('#frmFiles'),
                        success: function(data){
                            $('#cboFiles').html(data.options);
                            $('#txtCountFiles').val(data.numberFiles);
                }
                        
            });
                
                }
                
                    $('#btnDeleteFile').click(function(){
                    $.ajax({
                        url: '../queries/delete-file.php',//url sin comillas-->
                        type: 'POST',
                        dataType: 'json',
                        data: {'fileName': $('#cboFiles').val()},
                        success: function(data){
                        $('#dialog').html(data.message);
                        $('#dialog').dialog({
                                autoOpen: true,
                                modal: true,
                                buttons: {
                                    'Cerrar': function(){
                                        $(this).dialog('close');
                                    }
                                }
                            });
                             listFiles();
                        }
                    });
                 });
                

        </script>
    </body>
</html>
